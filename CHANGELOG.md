# Version 0.0.3
General Changelog pruning

## Features

### CycleList
Added `List.CycleNext(int index)` and `Array.CycleNext(int index)` extension Methods to safely cycle through those collections and get the first element instead of overflowing

# Version 0.0.2
## Features

### Angle to Vector2
Added `Helper.RadianAngleToVector2(float angle)` and `Helper.DegreeAngleToVector2(float angle)` to easily convert an angle to its `Vector2` equivalent.

### Uxml Creation
Added `Uxml.Create` and a generic overload to easily create UXML Elements and add a USS class to it

### Uxml Parenting
Added `VisualElement.AddChildren(params VisualElement[] children)` extension method to easily add multiple children to a UXML Element

# Version 0.0.1 (Initial Release)
## Features

### Camera Management
Added `Helper.Camera` property to easily access the main camera in the scene or find it if not cached.

### Coroutine Utilities
Added `Helper.GetWait(float time)` method to efficiently create and cache `WaitForSeconds` objects.

### GameObject Utilities
Added `Transform.DeleteChildren()` extension method to delete all child `GameObjects` of a specified `Transform`.

### UI Element Positioning
Added `Helper.GetWorldPositionOfCanvasElement(RectTransform element)` method to convert screen space positions of UI elements to world space positions.

### Audio Fading
Added several `AudioSource.FadeOut` extension methods to gradually fade out audio with customizable options.

This initial release provides essential utility functions to streamline common tasks in Unity game development. Please refer to the documentation for detailed usage instructions.
