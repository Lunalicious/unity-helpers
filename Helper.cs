﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public static class Helper
{
    private static Camera _camera;

    /// <summary>
    /// Returns the main camera in the scene or the cached camera reference if available.
    /// </summary>
    /// <remarks>
    /// If the cached camera reference is not available, it will try to find the main camera in the scene.
    /// </remarks>
    /// <returns>The main camera.</returns>
    public static Camera Camera => _camera ??= Camera.main;

    private static readonly Dictionary<float, WaitForSeconds> WaitDictionary = new ();

    /// <summary>
    /// Returns a cached WaitForSeconds object with the specified time duration.
    /// </summary>
    /// <param name="time">The time duration to wait in seconds.</param>
    /// <returns>A WaitForSeconds object for waiting in coroutines.</returns>
    public static WaitForSeconds GetWait(float time)
    {
        if (WaitDictionary.TryGetValue(time, out var wait)) return wait;

        WaitDictionary[time] = new WaitForSeconds(time);
        return WaitDictionary[time];
    }

    /// <summary>
    /// Destroys all child GameObjects of the given transform.
    /// </summary>
    /// <param name="t">The transform whose children should be removed.</param>
    public static void DeleteChildren(this Transform t)
    {
        foreach (Transform child in t)
        {
            Object.Destroy(child.gameObject);
        }
    }

    /// <summary>
    /// Converts the screen space position of a UI element to a world space position using the specified camera.
    /// </summary>
    /// <param name="element">The RectTransform of the UI element.</param>
    /// <returns>The world space position of the UI element.</returns>
    public static Vector2 GetWorldPositionOfCanvasElement(RectTransform element)
    {
        RectTransformUtility.ScreenPointToWorldPointInRectangle(element, element.position, Camera, out var result);
        return result;
    }

    /// <summary>
    /// Fades the audio source out gradually over a specified number of seconds.
    /// </summary>
    /// <param name="source">The AudioSource to fade out.</param>
    /// <param name="seconds">The duration (in seconds) over which the AudioSource volume should fade out.</param>
    /// <returns>A coroutine that can be used with StartCoroutine().</returns>
    public static IEnumerator FadeOut(this AudioSource source, float seconds)
    {
        if (!source.isPlaying || source.volume == 0)
            yield break;

        const float startVolume = 1.0f;
        var elapsedTime = 0f;

        while (source.volume > 0)
        {
            yield return GetWait(seconds * Time.deltaTime);

            elapsedTime += Time.deltaTime;
            var t = Mathf.Clamp01(elapsedTime / seconds);

            source.volume = Mathf.Lerp(startVolume, 0, t);
        }

        source.Stop();
        source.time = 0;
    }
    
    /// <summary>
    /// Fades the audio source out gradually over a specified number of seconds with customizable granularity.
    /// </summary>
    /// <param name="source">The AudioSource to fade out.</param>
    /// <param name="seconds">The duration (in seconds) over which the AudioSource volume should fade out.</param>
    /// <param name="granularity">The granularity of volume reduction for smooth fading. Smaller values result in smoother fading.</param>
    /// <returns>A coroutine that can be used with StartCoroutine().</returns>
    public static IEnumerator FadeOut(this AudioSource source, float seconds, float granularity)
    {
        if (!source.isPlaying || source.volume == 0)
            yield break;

        var elapsedTime = 0f;
        const float startVolume = 1f;

        while (source.volume > 0)
        {
            yield return GetWait(seconds * granularity);

            elapsedTime += seconds * granularity;
            var t = Mathf.Clamp01(elapsedTime / seconds);

            source.volume = Mathf.Lerp(startVolume, 0, t);
        }

        source.Stop();
        source.time = 0;
    }

    /// <summary>
    /// Fades the audio source out gradually over a specified number of seconds with customizable granularity.
    /// </summary>
    /// <param name="source">The AudioSource to fade out.</param>
    /// <param name="seconds">The duration (in seconds) over which the AudioSource volume should fade out.</param>
    /// <param name="granularity">The granularity of volume reduction for smooth fading. Smaller values result in smoother fading.</param>
    /// <param name="stopAfterFade">If true, the audio source will be stopped after fading out; otherwise, it will keep playing at volume zero.</param>
    /// <returns>A coroutine that can be used with StartCoroutine().</returns>
    public static IEnumerator FadeOut(this AudioSource source, float seconds, float granularity, bool stopAfterFade)
    {
        if (!source.isPlaying || source.volume == 0)
            yield break;

        var elapsedTime = 0f;
        const float startVolume = 1f;

        while (source.volume > 0)
        {
            yield return GetWait(seconds * granularity);

            elapsedTime += seconds * granularity;
            var t = Mathf.Clamp01(elapsedTime / seconds);

            source.volume = Mathf.Lerp(startVolume, 0, t);
        }

        if (stopAfterFade)
        {
            source.Stop();
            source.time = 0;
        }
    }

    /// <summary>
    /// Fades the audio source out gradually over a specified number of seconds with customizable granularity and volume reset option.
    /// </summary>
    /// <param name="source">The AudioSource to fade out.</param>
    /// <param name="seconds">The duration (in seconds) over which the AudioSource volume should fade out.</param>
    /// <param name="granularity">The granularity of volume reduction for smooth fading. Smaller values result in smoother fading.</param>
    /// <param name="stopAfterFade">If true, the audio source will be stopped after fading out; otherwise, it will keep playing at volume zero.</param>
    /// <param name="resetVolume">If true, the original volume of the audio source will be saved and restored after fading out.</param>
    /// <returns>A coroutine that can be used with StartCoroutine().</returns>
    public static IEnumerator FadeOut(this AudioSource source, float seconds, float granularity,
                    bool stopAfterFade, bool resetVolume)
    {
        if (!source.isPlaying || source.volume == 0)
            yield break;

        var elapsedTime = 0f;
        const float startVolume = 1f;
        var originalVolume = source.volume;

        while (source.volume > 0)
        {
            yield return GetWait(seconds * granularity);

            elapsedTime += seconds * granularity;
            var t = Mathf.Clamp01(elapsedTime / seconds);

            source.volume = Mathf.Lerp(startVolume, 0, t);
        }

        if (resetVolume)
        {
            source.volume = originalVolume;
        }

        if (stopAfterFade)
        {
            source.Stop();
            source.time = 0;
        }
    }
    
    /// <summary>
    /// Converts a radian angle to a 2D vector representing the direction of that angle.
    /// </summary>
    /// <param name="angle">The angle in radians to convert.</param>
    /// <returns>A 2D vector representing the direction of the specified radian angle.</returns>
    public static Vector2 RadianAngleToVector2(float angle)
    {
        return new Vector2 (Mathf.Sin(angle), Mathf.Cos(angle));
    }
    
    /// <summary>
    /// Converts a degree angle to a 2D vector representing the direction of that angle.
    /// </summary>
    /// <param name="angle">The angle in degrees to convert.</param>
    /// <returns>A 2D vector representing the direction of the specified degree angle.</returns>
    public static Vector2 DegreeAngleToVector2(float angle)
    {
        var radAngle = angle * Mathf.Deg2Rad;
        return RadianAngleToVector2(radAngle);
    }

    /// <summary>
    /// Retrieves the next element in a cycle from the specified list based on the current index.
    /// </summary>
    /// <typeparam name="T">The type of elements in the list.</typeparam>
    /// <param name="list">The list from which to retrieve the next element.</param>
    /// <param name="index">The current index in the list.</param>
    /// <returns>The next element in the cycle.</returns>
    /// <remarks>
    /// If the index is at the end of the list, it wraps around to the beginning, creating a cycle.
    /// </remarks>
    public static T CycleNext<T>(this IList<T> list, int index) => list[++index % list.Count];

    /// <summary>
    /// Retrieves the next element in a cycle from the specified array based on the current index.
    /// </summary>
    /// <typeparam name="T">The type of elements in the array.</typeparam>
    /// <param name="arr">The array from which to retrieve the next element.</param>
    /// <param name="index">The current index in the array.</param>
    /// <returns>The next element in the cycle.</returns>
    /// <remarks>
    /// If the index is at the end of the array, it wraps around to the beginning, creating a cycle.
    /// </remarks>
    public static T CycleNext<T>(this T[] arr, int index) => arr[++index % arr.Length];

}

public static class Uxml
{
    /// <summary>
    /// Creates a VisualElement of the specified type and adds the specified USS class to it.
    /// </summary>
    /// <typeparam name="T">The type of VisualElement to create.</typeparam>
    /// <param name="ussClass">A USS class name to add to the created VisualElement.</param>
    /// <returns>The created VisualElement.</returns>
    public static T Create<T>(string ussClass = "") where T : VisualElement, new()
    {
        var element = new T();

        element.AddToClassList(ussClass);

        return element;
    }

    /// <summary>
    /// Creates a generic VisualElement and adds the specified USS class to it.
    /// </summary>
    /// <param name="ussClass">A USS class name to add to the created VisualElement.</param>
    /// <returns>The created VisualElement.</returns>
    public static VisualElement Create(string ussClass = "") => Create<VisualElement>(ussClass);

    /// <summary>
    /// Adds an array of child VisualElements to the specified parent VisualElement.
    /// </summary>
    /// <param name="root">The parent VisualElement to which children should be added.</param>
    /// <param name="children">An array of child VisualElements to add to the parent.</param>
    /// <returns>The parent VisualElement with added children for convenience.</returns>
    public static VisualElement AddChildren(this VisualElement root, params VisualElement[] children)
    {
        foreach (var child in children)
        {
            root.Add(child);
        }

        return root;
    }
}
