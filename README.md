# Unity Helper Library

The Unity Helper Library is a collection of utility functions and extensions to simplify common tasks in Unity game development. It provides convenient methods for tasks such as camera management, object destruction, audio fading, UI element positioning, and more. By using this library, you can streamline your Unity development process and make your code cleaner and more efficient.

[[_TOC_]]

## Installation

To use the Unity Helper Library in your Unity project, follow these steps:

    Download: Clone or download this Git repository.

    Import into Unity:
        Open your Unity project.
        Go to Assets > Import Package > Custom Package...
        Navigate to the downloaded repository folder and select the UnityHelperLibrary.unitypackage file. (I will add these soon <3 Until then, just get the files via clone or download and put them anywhere into your project)
        Click "Open" to import the package into your project.

## Contributing

Contributions to the Unity Helper Library are welcome! If you'd like to contribute, please follow these guidelines:

    Fork the repository.

    Create a new branch for your feature or bug fix.

    Make your changes and test them thoroughly.

    Ensure your code follows the project's coding standards.

    Submit a merge request with a clear description of your changes.

    Don't forget the Changelog. I'm sure I myself will forget it though...

    Your merge request will be reviewed, and your contributions will be considered for merging.


## License

The Unity Helper Library is released under the MIT License. You are free to use, modify, and distribute this library in your projects. See the LICENSE file for more details.
